/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(".wish-icon i").click(function () {
        $(this).toggleClass("fa-heart fa-heart-o");
    });
    $('.banner_search_form .form').find('input, textarea').on('keyup blur focus', function (e) {
        var $this = $(this),
                label = $this.prev('label');
        if (e.type === 'keyup') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        } else if (e.type === 'focus') {
            if ($this.val() === '') {
                label.removeClass('highlight');
            } else if ($this.val() !== '') {
                label.addClass('highlight');
            }
        }

    });

    $('.banner_search_form .tab a').on('click', function (e) {
        e.preventDefault();
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
        target = $(this).attr('href');
        $('.tab-content > div').not(target).hide();
        $(target).fadeIn(600);
    });
    $('input[type=radio]').on('click', function (e) {
        var clickValue = $(this).attr("id");
        console.log(clickValue);
        if(clickValue == 'radio-bybudget'){
            $('#search-by-budget').show();
            $('#search-by-brand').hide();
        }else if(clickValue == 'radio-bybrand'){
            $('#search-by-budget').hide();
            $('#search-by-brand').show();
        }
        
        if(clickValue == 'radio-bybudget-usedcar'){
            $('#search-by-budget-usedcar').show();
            $('#search-by-brand-usedcar').hide();
        }else if(clickValue == 'radio-bybrand-usedcar'){
            $('#search-by-budget-usedcar').hide();
            $('#search-by-brand-usedcar').show();
        }
    });
});


