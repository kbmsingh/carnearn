$(document).ready(function () {
    $(function () {
        $("#new-car-brand").on("change", function () {
            var selectedVal = $(this).val();
            $.ajax({
                cache: false,
                url: 'json/brand_model.json',
                dataType: 'JSON',
                success: function (data) {
                    $("#new-car-model").html('');
                    $.each(data, function (key, val) {
                        if (key == selectedVal) {
                            $.each(val, function (keys, vals) {                                
                                $("#new-car-model").append('<option value="' + vals + '">' + vals + '</option>');
                            });
                        }
                    });
                },
                error: function () {
                    $("#new-car-model").html('<option value="-1">none available</option>');
                }
            });
        });
        $('#brand-model-search').typeahead({
            limit: 10,
            minLength: 1,            
            source: function (query, result) {
                $.ajax({
                    cache: false,
                    url: "json/brand_model.json",
                    data: 'query=' + query,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        var resultList = [];
                        $.map(data,function (key,value) {
                            resultList.push(value);
                            $.map(key,function (keys) {
                                if(keys != 'Select Model'){
                                    parentVal = value;
                                    childVal = keys;
                                    resultList.push(value +" - "+keys);
                                }
                            });
                        });
                        return result(resultList);
                    }
                });                
            },
            afterSelect: function(item) {
                console.log(item);
                window.location.href = "search/?value="+item;
            }
        });
    });
});

