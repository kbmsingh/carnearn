<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Car Search</title>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style_home.css" type="text/css"/>        
        <link rel="stylesheet" href="css/style_login.css" type="text/css"/>        
        <link rel="stylesheet" href="css/style_register.css" type="text/css"/>        
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <!-- Bootstrap files (jQuery first, then Popper.js, then Bootstrap JS) -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" type="text/javascript"></script>   
        <script src="js/bootstrap3-typeahead.min.js" type="text/javascript"></script> 
        <script src="js/script_home.js" type="text/javascript"></script> 
        <script src="js/json_load.js" type="text/javascript"></script> 
    </head>
    <body>
        <div id="header_nav" class="row">
            <nav class="navbar navbar-expand-xl navbar-dark">
                <div class="col-lg-2">
                    <a href="#" class="navbar-brand"><img src="images/car-logo.png"/></a>  
                </div>
                <div class="collapse navbar-collapse justify-content-start col-lg-8">		
                    <form class="navbar-form form-inline search-form">
                        <div class="input-group">
                            <input type="text" id="brand-model-search" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="collapse navbar-collapse justify-content-start col-lg-2">
                    <div class="navbar-nav ml-auto">
                        <div class="nav-item login-dropdown">
                            <i class="fa fa-user-o"></i> <a href="#carnearn-login" class="trigger-btn" data-toggle="modal">Login</a> / <a href="#" data-toggle="modal" data-target="#carnearn-register" data-title="Car nearn register">Register</a>                            
                        </div>			
                    </div>
                </div>
            </nav>
        </div>
        <div id="header_nav_menu">
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">NEW CAR</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">Search New Cars</a></li>
                                <li><a class="dropdown-item" href="#">Latest Cars</a></li>
                                <li><a class="dropdown-item" href="#">Popular Cars</a></li>
                                <li><a class="dropdown-item" href="#">Upcoming Cars</a></li>
                                <li><a class="dropdown-item" href="#">Suggest Me a Car</a></li>
                                <li><a class="dropdown-item" href="#">BS6 Cars</a></li>
                                <li><a class="dropdown-item" href="#">Electric Cars</a></li>
                                <li><a class="dropdown-item" href="#">Offers and Discount</a></li>
                                <li><a class="dropdown-item" href="#">Service Centers</a></li>
                                <li><a class="dropdown-item" href="#">Dealers</a></li>
                                <li><a class="dropdown-item" href="#">Win iPhone</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">USED CAR</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down ls-width">
                                <li><a class="dropdown-item" href="#">Cars In Your City</a></li>
                                <li><a class="dropdown-item" href="#">Search Used Cars</a></li>
                                <li><a class="dropdown-item" href="#">Sell Used Cars</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Valuation</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Dealers</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Loan</a></li>
                                <li><a class="dropdown-item" href="#">Trustmark Cars</a></li>
                                <li><a class="dropdown-item" href="#">My Listing</a></li>
                                <li><a class="dropdown-item" href="#">CarDekho Trustmark Dealers</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#">CarDekho Gaadi Store</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">SELL CAR</a></li>   
                        <li class="nav-item"><a class="nav-link" href="#">COMPARE CARS</a></li>   
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">NEWS & REVIEWS</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">Car News</a></li>
                                <li><a class="dropdown-item" href="#">Feature Stories</a></li>
                                <li><a class="dropdown-item" href="#">Car Collections</a></li>
                                <li><a class="dropdown-item" href="#">Auto Expo</a></li>
                                <li><a class="dropdown-item" href="#">User Reviews</a></li>
                                <li><a class="dropdown-item" href="#">Road Test</a></li>
                                <li><a class="dropdown-item" href="#">Video Reviews</a></li>
                                <li><a class="dropdown-item" href="#">Write a Review</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">Cardekho Ventures</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">CarDekho Gaadi Store</a></li>
                                <li><a class="dropdown-item" href="#">Car Insurance</a></li>
                                <li><a class="dropdown-item" href="#">Health Insurance</a></li>
                                <li><a class="dropdown-item" href="#">Loan Against Car</a></li>
                                <li><a class="dropdown-item" href="#">Car Tyres</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">MORE</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down ls-width-1">
                                <li><a class="dropdown-item" href="#">Car Loan</a></li>
                                <li><a class="dropdown-item" href="#">EMI Calculator</a></li>
                                <li><a class="dropdown-item" href="#">Ask A Question</a></li>
                                <li><a class="dropdown-item" href="#">Electric Zone <br/>By MG Motor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> <!-- navbar-collapse.// -->
            </nav>
        </div>
        <div id="carousel">
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                            <li data-target="#myCarousel" data-slide-to="5"></li>
                            <li data-target="#myCarousel" data-slide-to="6"></li>
                            <li data-target="#myCarousel" data-slide-to="7"></li>
                            <li data-target="#myCarousel" data-slide-to="8"></li>
                            <li data-target="#myCarousel" data-slide-to="9"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="images/slider-1.png" class="img-fluid" alt="Noncompressedmgweb">
                                <div class="carousel-caption">
                                    <h3>Award Winning Support</h3>
                                    <p>Pulvinar leo id risus pellentesque vestibulum. Sed diam libero, sodales eget cursus dolor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>	
                            <div class="carousel-item">
                                <img src="images/slider-2.png" class="img-fluid" alt="Jeep-Compass-0">
                                <div class="carousel-caption">
                                    <h3>Amazing Digital Experience</h3>							
                                    <p>Nullam hendrerit justo non leo aliquet imperdiet. Etiam sagittis lectus condime dapibus.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-3.png" class="img-fluid" alt="DSK-0">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-4.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-5.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-6.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-7.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-8.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-9.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/slider-10.png" class="img-fluid" alt="AmazeWeb">
                                <div class="carousel-caption">
                                    <h3>Live Monitoring Tools</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu pellentesque sem tempor.</p>
                                    <div class="carousel-action">
                                        <a href="#" class="btn btn-primary">Learn More</a>
                                        <a href="#" class="btn btn-success">Try Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row banner_search_form">
                <div class="form">      
                    <h1>Find your right car</h1>
                    <div class="tab-content">
                        <ul class="tab-group">
                            <li class="tab active"><a href="#banner-form-newcar">New Car</a></li>
                            <li class="tab"><a href="#banner-form-usedcar">Used Car</a></li>
                        </ul>
                        <div id="banner-form-newcar">
                            <form action="/" method="post">          
                                <div class="top-row">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="radio-bybudget" name="newcarsearchtype" checked>
                                        <label class="custom-control-label" for="radio-bybudget">By Budget</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="radio-bybrand" name="newcarsearchtype">
                                        <label class="custom-control-label" for="radio-bybrand">By Brand</label>
                                    </div>
                                </div>
                                <div id="search-by-budget">
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>1 - 5 Lakh</option>
                                            <option>5 - 10 Lakh</option>
                                            <option>10 - 15 Lakh</option>
                                            <option>15 - 20 Lakh</option>
                                            <option>20 - 50 Lakh</option>
                                            <option>50 Lakh - 1 Crore</option>
                                            <option>Above 1 Crore</option>
                                        </select>
                                    </div>          
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>Hatchback</option>
                                            <option>Sedan</option>
                                            <option>SUV</option>
                                            <option>MUV</option>
                                            <option>Luxury</option>
                                            <option>Super Luxury</option>
                                            <option>Convertible</option>
                                            <option>Hybrid</option>
                                            <option>Coupe</option>
                                            <option>Pickup Truck</option>
                                            <option>Minivan</option>
                                            <option>Wagon</option>
                                        </select>
                                    </div>  
                                </div>
                                <div id="search-by-brand">
                                    <div class="field-wrap">
                                        <select id="new-car-brand" class="custom-select">
                                            <option value="Select Brand">Select Brand</option>
                                            <option value="Popular Brands">Popular Brands</option>
                                            <option value="Maruti">Maruti</option>
                                            <option value="Hyundai">Hyundai</option>
                                            <option value="Honda">Honda</option>
                                            <option value="Tata">Tata</option>
                                            <option value="Mahindra">Mahindra</option>
                                            <option value="Renault">Renault</option>
                                            <option value="Ford">Ford</option>
                                            <option value="Nissan">Nissan</option>
                                            <option value="Datsun">Datsun</option>
                                            <option value="Toyota">Toyota</option>
                                            <option value="MG">MG</option>
                                            <option value="Kia">Kia</option>
                                            <option value="All Brands">All Brands</option>
                                            <option value="Aston Martin">Aston Martin</option>
                                            <option value="Audi">Audi</option>
                                            <option value="Bentley">Bentley</option>
                                            <option value="BMW">BMW</option>
                                            <option value="Bugatti">Bugatti</option>
                                            <option value="Citroen">Citroen</option>
                                            <option value="DC">DC</option>
                                            <option value="Ferrari">Ferrari</option>
                                            <option value="Fiat">Fiat</option>
                                            <option value="Force">Force</option>
                                            <option value="Haima">Haima</option>
                                            <option value="Haval">Haval</option>
                                            <option value="Isuzu">Isuzu</option>
                                            <option value="Jaguar">Jaguar</option>
                                            <option value="Jeep">Jeep</option>
                                            <option value="Lamborghini">Lamborghini</option>
                                            <option value="Land Rover">Land Rover</option>
                                            <option value="Lexus">Lexus</option>
                                            <option value="Maserati">Maserati</option>
                                            <option value="Mercedes-Benz">Mercedes-Benz</option>
                                            <option value="Mini">Mini</option>
                                            <option value="Mitsubishi">Mitsubishi</option>
                                            <option value="ORA">ORA</option>
                                            <option value="Porsche">Porsche</option>
                                            <option value="Rolls-Royce">Rolls-Royce</option>
                                            <option value="Skoda">Skoda</option>
                                            <option value="Tesla">Tesla</option>
                                            <option value="Volkswagen">Volkswagen</option>
                                            <option value="Volvo">Volvo</option>
                                        </select>
                                    </div>          
                                    <div class="field-wrap">
                                        <select id="new-car-model" class="custom-select">
                                            <option> Select Model</option>
                                            <option> Alto 800</option>
                                            <option> Baleno</option>
                                            <option> Celerio</option>
                                            <option> Celerio X</option>
                                            <option> Ciaz</option>
                                            <option> Dzire</option>
                                            <option> Eeco</option>
                                            <option> Ertiga</option>
                                            <option> Ignis</option>
                                            <option> S-Presso</option>
                                            <option> Swift</option>
                                            <option> Vitara Brezza</option>
                                            <option> Wagon R</option>
                                            <option> XL6</option>
                                            <option> Upcoming Model</option>
                                            <option> Futuro-e</option>
                                            <option> Grand Vitara</option>
                                            <option> Jimny</option>
                                            <option> S-Cross 2020</option>
                                            <option> Solio</option>
                                            <option> Swift 2020</option>
                                            <option> Swift Hybrid</option>
                                            <option> WagonR Electric</option>
                                            <option> XL5</option>
                                        </select>
                                    </div>  
                                </div>
                                <button type="submit" class="button button-block">Search</button>          
                            </form>
                        </div>        
                        <div id="banner-form-usedcar"> 
                            <form action="/" method="post">          
                                <div class="top-row">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="radio-bybudget-usedcar" name="usedcarsearchtype" checked>
                                        <label class="custom-control-label" for="radio-bybudget">By Budget</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="radio-bybrand-usedcar" name="usedcarsearchtype">
                                        <label class="custom-control-label" for="radio-bybrand">By Brand</label>
                                    </div>
                                </div>
                                <div id="search-by-budget-usedcar">
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>1 - 5 Lakh</option>
                                            <option>5 - 10 Lakh</option>
                                            <option>10 - 15 Lakh</option>
                                            <option>15 - 20 Lakh</option>
                                            <option>20 - 50 Lakh</option>
                                            <option>50 Lakh - 1 Crore</option>
                                            <option>Above 1 Crore</option>
                                        </select>
                                    </div> 
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>Hatchback</option>
                                            <option>Sedan</option>
                                            <option>SUV</option>
                                            <option>MUV</option>
                                            <option>Luxury</option>
                                            <option>Super Luxury</option>
                                            <option>Convertible</option>
                                            <option>Hybrid</option>
                                            <option>Coupe</option>
                                            <option>Pickup Truck</option>
                                            <option>Minivan</option>
                                            <option>Wagon</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="search-by-brand-usedcar">
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>Select Brand</option>
                                            <option>1 - 5 Lakh</option>
                                            <option>5 - 10 Lakh</option>
                                            <option>10 - 15 Lakh</option>
                                            <option>15 - 20 Lakh</option>
                                            <option>20 - 50 Lakh</option>
                                            <option>50 Lakh - 1 Crore</option>
                                            <option>Above 1 Crore</option>
                                        </select>
                                    </div>          
                                    <div class="field-wrap">
                                        <select class="custom-select">
                                            <option>Select Model</option>
                                            <option>Hatchback</option>
                                            <option>Sedan</option>
                                            <option>SUV</option>
                                            <option>MUV</option>
                                            <option>Luxury</option>
                                            <option>Super Luxury</option>
                                            <option>Convertible</option>
                                            <option>Hybrid</option>
                                            <option>Coupe</option>
                                            <option>Pickup Truck</option>
                                            <option>Minivan</option>
                                            <option>Wagon</option>
                                        </select>
                                    </div>                                    
                                </div>     
                                <button type="submit" class="button button-block">Search</button>           
                            </form>
                        </div>        
                    </div><!-- tab-content -->      
                </div> <!-- /form -->
            </div>
        </div>  
        <div id="carousel_second">
            <div class="row row-1">
                <div>
                    <h2>Recommended Cars For You</h2>
                    <div id="myCarouselSecond_1" class="carousel slide" data-ride="carousel" data-interval="0">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="">									
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPad</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$400.00</strike> <b>$369.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="Headphone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Headphone</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$25.00</strike> <b>$23.99</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>		
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_5.png" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Air</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$899.00</strike> <b>$649.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>								
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_6.png" class="img-fluid" alt="Nikon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Nikon DSLR</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$315.00</strike> <b>$250.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_7.png" class="img-fluid" alt="Play Station">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Play Station</h4>
                                                <p class="item-price"><strike>$289.00</strike> <span>$269.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_1.jpg" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Pro</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$869.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="Speaker">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Bose Speaker</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$99.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="Galaxy">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Samsung Galaxy S8</h4>
                                                <p class="item-price"><strike>$599.00</strike> <span>$569.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>						
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="iPhone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPhone</h4>
                                                <p class="item-price"><strike>$369.00</strike> <span>$349.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_1.jpg" class="img-fluid" alt="Canon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Canon DSLR</h4>
                                                <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="Pixel">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Google Pixel</h4>
                                                <p class="item-price"><strike>$450.00</strike> <span>$418.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>	
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="Watch">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple Watch</h4>
                                                <p class="item-price"><strike>$350.00</strike> <span>$330.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control-prev" href="#myCarouselSecond_1" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#myCarouselSecond_1" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>            
            <div class="row row-3">
                <div>
                    <h2>Latest Cars</h2>
                    <div id="myCarouselSecond_3" class="carousel slide" data-ride="carousel" data-interval="0">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarouselSecond_3" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarouselSecond_3" data-slide-to="1"></li>
                            <li data-target="#myCarouselSecond_3" data-slide-to="2"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="">									
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPad</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$400.00</strike> <b>$369.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="Headphone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Headphone</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$25.00</strike> <b>$23.99</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>		
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_5.png" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Air</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$899.00</strike> <b>$649.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>								
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_6.png" class="img-fluid" alt="Nikon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Nikon DSLR</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$315.00</strike> <b>$250.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_7.png" class="img-fluid" alt="Play Station">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Play Station</h4>
                                                <p class="item-price"><strike>$289.00</strike> <span>$269.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_1.jpg" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Pro</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$869.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_2.jpg" class="img-fluid" alt="Speaker">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Bose Speaker</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$99.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="Galaxy">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Samsung Galaxy S8</h4>
                                                <p class="item-price"><strike>$599.00</strike> <span>$569.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>						
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="iPhone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPhone</h4>
                                                <p class="item-price"><strike>$369.00</strike> <span>$349.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_5.png" class="img-fluid" alt="Canon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Canon DSLR</h4>
                                                <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_6.png" class="img-fluid" alt="Pixel">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Google Pixel</h4>
                                                <p class="item-price"><strike>$450.00</strike> <span>$418.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>	
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_7.png" class="img-fluid" alt="Watch">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple Watch</h4>
                                                <p class="item-price"><strike>$350.00</strike> <span>$330.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control-prev" href="#myCarouselSecond_3" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#myCarouselSecond_3" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="brand-name" class="row row-3">
                <div>
                    <h2>Popular <b>Brands</b></h2>
                    <div id="brand-name-icon" class="carousel slide" data-ride="carousel" data-interval="0">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#brand-name-icon" data-slide-to="0" class="active"></li>
                            <li data-target="#brand-name-icon" data-slide-to="1"></li>
                            <li data-target="#brand-name-icon" data-slide-to="2"></li>
                            <li data-target="#brand-name-icon" data-slide-to="3"></li>
                            <li data-target="#brand-name-icon" data-slide-to="4"></li>
                            <li data-target="#brand-name-icon" data-slide-to="5"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/audi.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/bmw.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/datsun.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/ford.jpg" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/honda.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/hyundai.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/isuzu.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/jaguar.png" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/jeep.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/kia.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/landrover.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/mahindra.png" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/maruti.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/mercedes.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/mg.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/nissan.jpg" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/renault.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/skoda_1.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/skoda_2.jpg" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/toyota.png" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/toyota_1.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/ww.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/audi.png" class="img-fluid" alt=""></div></div>
                                    <div class="col-sm-3"><div class="img-box"><img src="images/brand_logo/honda.png" class="img-fluid" alt=""></div></div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control-prev" href="#brand-name-icon" data-slide="prev">
                            <i class="fa fa-chevron-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#brand-name-icon" data-slide="next">
                            <i class="fa fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row row-2">
                <div>
                    <h2>The most searched cars</h2>
                    <div id="myCarouselSecond_2" class="carousel slide" data-ride="carousel" data-interval="0">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarouselSecond_2" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarouselSecond_2" data-slide-to="1"></li>
                            <li data-target="#myCarouselSecond_2" data-slide-to="2"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="">									
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPad</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$400.00</strike> <b>$369.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="Headphone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Headphone</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$25.00</strike> <b>$23.99</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>		
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_5.png" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Air</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$899.00</strike> <b>$649.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>								
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_6.png" class="img-fluid" alt="Nikon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Nikon DSLR</h4>									
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="item-price"><strike>$315.00</strike> <b>$250.00</b></p>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_7.png" class="img-fluid" alt="Play Station">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Sony Play Station</h4>
                                                <p class="item-price"><strike>$289.00</strike> <span>$269.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_1.jpg" class="img-fluid" alt="Macbook">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Macbook Pro</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$869.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_2.jpg" class="img-fluid" alt="Speaker">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Bose Speaker</h4>
                                                <p class="item-price"><strike>$109.00</strike> <span>$99.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_3.jpg" class="img-fluid" alt="Galaxy">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Samsung Galaxy S8</h4>
                                                <p class="item-price"><strike>$599.00</strike> <span>$569.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>						
                                </div>
                            </div>
                            <div class="item carousel-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_4.png" class="img-fluid" alt="iPhone">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple iPhone</h4>
                                                <p class="item-price"><strike>$369.00</strike> <span>$349.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_5.png" class="img-fluid" alt="Canon">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Canon DSLR</h4>
                                                <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_6.png" class="img-fluid" alt="Pixel">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Google Pixel</h4>
                                                <p class="item-price"><strike>$450.00</strike> <span>$418.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-half-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>	
                                    <div class="col-sm-3">
                                        <div class="thumb-wrapper">
                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                            <div class="img-box">
                                                <img src="images/car_7.png" class="img-fluid" alt="Watch">
                                            </div>
                                            <div class="thumb-content">
                                                <h4>Apple Watch</h4>
                                                <p class="item-price"><strike>$350.00</strike> <span>$330.00</span></p>
                                                <div class="star-rating">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                                        <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                                                    </ul>
                                                </div>
                                                <a href="#" class="btn btn-primary">Add to Cart</a>
                                            </div>						
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control-prev" href="#myCarouselSecond_2" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control-next" href="#myCarouselSecond_2" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div id="compare-to-buy-car" class="row row-3">
                <div>
                    <h2>Compare to buy the right car</h2>
                    <div class="card-columns">
                        <div class="card">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <p class="vs">VS</p>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Vestibulum id metus ac nisl bibendum scelerisque non dignissim purus.</p>
                                <p class="card-text"><small class="text-muted">Last updated 2 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <p class="vs">VS</p>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Vestibulum id metus ac nisl bibendum scelerisque non dignissim purus.</p>
                                <p class="card-text"><small class="text-muted">Last updated 2 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                            <p class="vs">VS</p>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Vestibulum id metus ac nisl bibendum scelerisque non dignissim purus.</p>
                                <p class="card-text"><small class="text-muted">Last updated 2 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="get-trusted-used-cars" class="row row-3">
                <div>
                    <h2>Get trusted used cars nearby</h2>
                    <div class="col-sm-8">
                        <div class="card-group">
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Ahmedabad</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Bangalore</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Chennai</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Delhi NCR</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Gurgaon</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Hyderabad</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-group">
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Jaipur</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Kolkata</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Mumbai</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">New Delhi</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Noida</h5>
                                </div>
                            </div>
                            <div class="card">
                                <img src="images/thumbnail.svg" class="card-img-top" alt="...">
                                <div class="card-body">                                
                                    <p class="card-text">Used cars in</p>
                                    <h5 class="card-title">Pune</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <p>I am looking to buy a second hand car in</p>
                        <form>
                            <select class="custom-select">
                                <option selected>Enter your city</option>
                                <option value="1">Delhi</option>
                                <option value="2">Noida</option>
                                <option value="3">Three</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            <div id="news-help-choose-car" class="row row-3">
                <h2>News to help choose your car</h2>
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mar-left">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mar-left">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="know-more-choose-better" class="row row-3">
                <h2>Know more to choose better</h2>
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mar-left">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mar-left">
                    <div class="row no-gutters">
                        <div class="col-sm-5" style="background: #868e96;">
                            <img src="images/thumbnail.svg" class="card-img-top h-100" alt="...">
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title">Alice Liddel</h5>
                                <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     
        </div>
        <div id="foot_top_menu" class="row">
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">India’s #1 <small><i>Largest Auto portal</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Car Sold <small><i>Every 4 minute</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Offers <small><i>Stay updated pay less</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Compare <small><i>Decode the right car</i></small></h5>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot_middle_menu" class="row row-3">
            <div class="col-md-3 col-sm-6">
                <div class="title">OVERVIEW</div>
                <ul>
                    <li><span>About us</span></li>
                    <li><span>FAQs</span></li>
                    <li><a href="https://www.cardekho.com/info/privacy_policy" title="Privacy Policy">Privacy Policy</a></li>
                    <li><a href="https://www.cardekho.com/info/terms_and_condition" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                    <li><a href="https://stimg.cardekho.com/policy/CSR_Policy.pdf" title="Corporate Policies" target="_blank" rel="noopener">Corporate Policies</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">OTHERS</div>
                <ul>
                    <li><span>Trustmarked used cars</span></li>
                    <li><span>Advertise with Us</span></li>
                    <li><span>Careers</span></li>
                    <li><span>Customer Care</span></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">CONNECT WITH US</div>
                <ul>
                    <li class="hoverremove"><span>XXXX XXX XXXX (Toll-Free)</span></li>
                    <li><span>xxxxxx@carnearn.com</span></li>
                    <li><span>Dealer solutions</span></li>
                    <li><span>Contact Us</span></li>
                    <li><span>Feedback</span></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">EXPERIENCE CARDEKHO APP</div>
                <ul>
                    <li><a href="" title="" target="_blank"></a></li>
                    <li><a href="" title="" target="_blank"></a></li>
                </ul>
            </div>
        </div>
        <div id="foot_bottom_menu" class="row">
            <div class="col-md-6">
                <p class="copyRight">© 2020 Carnearn Software Solution.</p>
            </div>
            <div class="col-md-6">
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="images/avatar.svg"></a>
            </div>
        </div>
        <!-- Modal HTML -->
        <div id="carnearn-login" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="avatar">
                            <img src="images/avatar.png" alt="Avatar">
                        </div>				
                        <h4 class="modal-title">Member Login</h4>	
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="/examples/actions/confirmation.php" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required="required">		
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">	
                            </div>        
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#">Forgot Password?</a>
                    </div>
                </div>
            </div>
        </div>   
        <!-- Modal HTML -->
        <div id="carnearn-register" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">				
                        <h4 class="modal-title">Register</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="/examples/actions/confirmation.php" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
                                    <div class="col"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
                                </div>        	
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
                            </div>        
                            <div class="form-group">
                                <label class="form-check-label"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
                            </div>
                        </form>
                        <div class="text-center">Already have an account? <a href="#">Sign in</a></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
