<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Car Search</title>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/style_home.css" type="text/css"/>        
        <link rel="stylesheet" href="../css/style_login.css" type="text/css"/>        
        <link rel="stylesheet" href="../css/style_register.css" type="text/css"/>        
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <!-- Bootstrap files (jQuery first, then Popper.js, then Bootstrap JS) -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" type="text/javascript"></script>   
        <script src="../js/bootstrap3-typeahead.min.js" type="text/javascript"></script> 
        <script src="../js/script_home.js" type="text/javascript"></script> 
        <script src="../js/json_load.js" type="text/javascript"></script> 
    </head>
    <body>
        <div id="header_nav" class="row">
            <nav class="navbar navbar-expand-xl navbar-dark">
                <div class="col-lg-2">
                    <a href="../" class="navbar-brand"><img src="../images/car-logo.png"/></a>  
                </div>
                <div class="collapse navbar-collapse justify-content-start col-lg-8">		
                    <form class="navbar-form form-inline search-form">
                        <div class="input-group">
                            <input type="text" id="brand-model-search" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="collapse navbar-collapse justify-content-start col-lg-2">
                    <div class="navbar-nav ml-auto">
                        <div class="nav-item login-dropdown">
                            <i class="fa fa-user-o"></i> <a href="#carnearn-login" class="trigger-btn" data-toggle="modal">Login</a> / <a href="#" data-toggle="modal" data-target="#carnearn-register" data-title="Car nearn register">Register</a>                            
                        </div>			
                    </div>
                </div>
            </nav>
        </div>
        <div id="header_nav_menu">
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">NEW CAR</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">Search New Cars</a></li>
                                <li><a class="dropdown-item" href="#">Latest Cars</a></li>
                                <li><a class="dropdown-item" href="#">Popular Cars</a></li>
                                <li><a class="dropdown-item" href="#">Upcoming Cars</a></li>
                                <li><a class="dropdown-item" href="#">Suggest Me a Car</a></li>
                                <li><a class="dropdown-item" href="#">BS6 Cars</a></li>
                                <li><a class="dropdown-item" href="#">Electric Cars</a></li>
                                <li><a class="dropdown-item" href="#">Offers and Discount</a></li>
                                <li><a class="dropdown-item" href="#">Service Centers</a></li>
                                <li><a class="dropdown-item" href="#">Dealers</a></li>
                                <li><a class="dropdown-item" href="#">Win iPhone</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">USED CAR</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down ls-width">
                                <li><a class="dropdown-item" href="#">Cars In Your City</a></li>
                                <li><a class="dropdown-item" href="#">Search Used Cars</a></li>
                                <li><a class="dropdown-item" href="#">Sell Used Cars</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Valuation</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Dealers</a></li>
                                <li><a class="dropdown-item" href="#">Used Car Loan</a></li>
                                <li><a class="dropdown-item" href="#">Trustmark Cars</a></li>
                                <li><a class="dropdown-item" href="#">My Listing</a></li>
                                <li><a class="dropdown-item" href="#">CarDekho Trustmark Dealers</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#">CarDekho Gaadi Store</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">SELL CAR</a></li>   
                        <li class="nav-item"><a class="nav-link" href="#">COMPARE CARS</a></li>   
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">NEWS & REVIEWS</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">Car News</a></li>
                                <li><a class="dropdown-item" href="#">Feature Stories</a></li>
                                <li><a class="dropdown-item" href="#">Car Collections</a></li>
                                <li><a class="dropdown-item" href="#">Auto Expo</a></li>
                                <li><a class="dropdown-item" href="#">User Reviews</a></li>
                                <li><a class="dropdown-item" href="#">Road Test</a></li>
                                <li><a class="dropdown-item" href="#">Video Reviews</a></li>
                                <li><a class="dropdown-item" href="#">Write a Review</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">Cardekho Ventures</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down">
                                <li><a class="dropdown-item" href="#">CarDekho Gaadi Store</a></li>
                                <li><a class="dropdown-item" href="#">Car Insurance</a></li>
                                <li><a class="dropdown-item" href="#">Health Insurance</a></li>
                                <li><a class="dropdown-item" href="#">Loan Against Car</a></li>
                                <li><a class="dropdown-item" href="#">Car Tyres</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown">MORE</a>
                            <ul class="dropdown-menu dropdown-menu-right fade-down ls-width-1">
                                <li><a class="dropdown-item" href="#">Car Loan</a></li>
                                <li><a class="dropdown-item" href="#">EMI Calculator</a></li>
                                <li><a class="dropdown-item" href="#">Ask A Question</a></li>
                                <li><a class="dropdown-item" href="#">Electric Zone <br/>By MG Motor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div> <!-- navbar-collapse.// -->
            </nav>
        </div> 
        <div id="carousel_second_page">
            <%
//                String searchValue = request.getParameter("value");
//                String[] splitValue = searchValue.split("-");
//                out.print("<br/>");
//                out.print("<h1>Brand Name: "+splitValue[0]+"</h1>");
//                out.print("<br/>");
//                out.print("<h1>Model Name: "+splitValue[1]+"</h1>");
//                out.print("<br/>");
//                out.print("<br/>");
//                out.print("<br/>");
//                out.print("<br/>");
            %>
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-sm-5" style="background: #868e96;">
                                <img src="../images/car_1.jpg" class="card-img-top h-100" alt="...">
                            </div>
                            <div class="col-sm-7">
                                <div class="card-body">
                                    <h5 class="card-title">Alice Liddel</h5>
                                    <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                    <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-sm-5" style="background: #868e96;">
                                <img src="../images/car_2.jpg" class="card-img-top h-100" alt="...">
                            </div>
                            <div class="col-sm-7">
                                <div class="card-body">
                                    <h5 class="card-title">Alice Liddel</h5>
                                    <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                    <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-sm-5" style="background: #868e96;">
                                <img src="../images/car_3.jpg" class="card-img-top h-100" alt="...">
                            </div>
                            <div class="col-sm-7">
                                <div class="card-body">
                                    <h5 class="card-title">Alice Liddel</h5>
                                    <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                    <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="col-sm-5" style="background: #868e96;">
                                <img src="../images/car_6.png" class="card-img-top h-100" alt="...">
                            </div>
                            <div class="col-sm-7">
                                <div class="card-body">
                                    <h5 class="card-title">Alice Liddel</h5>
                                    <p class="card-text">Alice is a freelance web designer and developer based in London. She is specialized in HTML5, CSS3, JavaScript, Bootstrap, etc.</p>
                                    <a href="#" class="btn btn-primary stretched-link">View Profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <ul class="list-group">
                        <li class="list-group-item">Pictures</li>
                        <li class="list-group-item">Documents</li>        
                        <li class="list-group-item">Music</li>
                        <li class="list-group-item">Videos</li>
                        <li class="list-group-item">Pictures</li>
                        <li class="list-group-item">Documents</li>        
                        <li class="list-group-item">Music</li>
                        <li class="list-group-item">Videos</li>
                    </ul>
                    <div class="card-group">
                        <div class="card">
                            <img src="../images/thumbnail.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                            </div>
                        </div>
                        <div class="card">
                            <img src="../images/thumbnail.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                            </div>
                        </div>
                        <div class="card">
                            <img src="../images/thumbnail.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot_top_menu" class="row">
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="../images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">India’s #1 <small><i>Largest Auto portal</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="../images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Car Sold <small><i>Every 4 minute</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="../images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Offers <small><i>Stay updated pay less</i></small></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="media">
                    <img src="../images/avatar.svg" class="rounded-circle" alt="Sample Image">
                    <div class="media-body">
                        <h5 class="mt-0">Compare <small><i>Decode the right car</i></small></h5>
                    </div>
                </div>
            </div>
        </div>
        <div id="foot_middle_menu" class="row row-3">
            <div class="col-md-3 col-sm-6">
                <div class="title">OVERVIEW</div>
                <ul>
                    <li><span>About us</span></li>
                    <li><span>FAQs</span></li>
                    <li><a href="https://www.cardekho.com/info/privacy_policy" title="Privacy Policy">Privacy Policy</a></li>
                    <li><a href="https://www.cardekho.com/info/terms_and_condition" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                    <li><a href="https://stimg.cardekho.com/policy/CSR_Policy.pdf" title="Corporate Policies" target="_blank" rel="noopener">Corporate Policies</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">OTHERS</div>
                <ul>
                    <li><span>Trustmarked used cars</span></li>
                    <li><span>Advertise with Us</span></li>
                    <li><span>Careers</span></li>
                    <li><span>Customer Care</span></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">CONNECT WITH US</div>
                <ul>
                    <li class="hoverremove"><span>XXXX XXX XXXX (Toll-Free)</span></li>
                    <li><span>xxxxxx@carnearn.com</span></li>
                    <li><span>Dealer solutions</span></li>
                    <li><span>Contact Us</span></li>
                    <li><span>Feedback</span></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="title">EXPERIENCE CARDEKHO APP</div>
                <ul>
                    <li><a href="" title="" target="_blank"></a></li>
                    <li><a href="" title="" target="_blank"></a></li>
                </ul>
            </div>
        </div>
        <div id="foot_bottom_menu" class="row">
            <div class="col-md-6">
                <p class="copyRight">© 2020 Carnearn Software Solution.</p>
            </div>
            <div class="col-md-6">
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="../images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="../images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="../images/avatar.svg"></a>
                <a href="" target="_blank" title="Carnearn"><img title="Carnearn" src="../images/avatar.svg"></a>
            </div>
        </div>
        <!-- Modal HTML -->
        <div id="carnearn-login" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="avatar">
                            <img src="../images/avatar.png" alt="Avatar">
                        </div>				
                        <h4 class="modal-title">Member Login</h4>	
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="/examples/actions/confirmation.php" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required="required">		
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">	
                            </div>        
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#">Forgot Password?</a>
                    </div>
                </div>
            </div>
        </div>   
        <!-- Modal HTML -->
        <div id="carnearn-register" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div class="modal-header">				
                        <h4 class="modal-title">Register</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form action="/examples/actions/confirmation.php" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
                                    <div class="col"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
                                </div>        	
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
                            </div>        
                            <div class="form-group">
                                <label class="form-check-label"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
                            </div>
                        </form>
                        <div class="text-center">Already have an account? <a href="#">Sign in</a></div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
